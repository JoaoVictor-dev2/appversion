<?php

namespace Sysout\AppVersion;

use GuzzleHttp\Client;

/**
 * Class Version
 *
 * @author João Victor <joaovictor@sysout.com.br>
 * @since 14/04/2022 
 * @version 1.0.0
 */
class Version
{

    private $bundleId;
    private $client;

    public function __construct($bundleId)
    {

        $this->bundleId = $bundleId;
        $this->client = new Client(
            [
                'timeout' => 10,
                'verify' => false
            ]
        );
    }

    /**
     * Retornar versão de um app ios
     *
     * @param string $packageName
     * @param string $country
     * @return $version
     */
    public function getIos($country = 'br')
    {

        try {

            $uri = "http://itunes.apple.com/lookup";

            $query = [
                'bundleId' => $this->bundleId,
                'country' => $country
            ];

            $options = [
                'query' => $query
            ];

            $response = $this->client->get($uri, $options);

            $body = $response->getBody();

            $json = $body->getContents();

            if ($json) {

                $data = json_decode($json);

                $version = $data->results[0]->version;

                if ($version) {

                    return $version;
                } else {

                    throw new \Exception('Não foi possível retornar o um valor válido');
                }
            } else {

                throw new \Exception('Não foi possível identificar a versão do app dentro do retorno');
            }
        } catch (\Exception $e) {

            // $message = $e->getMessage();

            return 'Não foi possível se conectar ao servidor / verifique sua conexão ';
        }
    }

    /**
     * Retornar versão de um app android
     *
     * @param string $bundleId
     * @return $result
     */
    public function getAndroid()
    {

        try {

            $uri = "https://play.google.com/store/apps/details";

            $query = [
                'id' => $this->bundleId,
            ];

            $options = [
                'query' => $query
            ];

            $response = $this->client->get($uri, $options);

            $body = $response->getBody();

            $html = $body->getContents();

            preg_match_all('/<span class="htlgb"><div class="IQ1z0d"><span class="htlgb">(.*?)<\/span><\/div><\/span>/s', $html, $output);

            $result = $output[1][3];

            if ($result) {

                return $result;
            } else {

                throw new \Exception('Não foi possível obter um resultado válido');
            }
        } catch (\Exception $e) {

            // Devolve erro de Curl
            // $message = $e->getMessage();

            return 'Não foi possível se conectar ao servidor';
        }
    }
}
