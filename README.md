# AppVersion

Installation
------------

The recommended way to install AppVersion is through Composer.

```bash
composer require sysout/appversion
```
Documentation
-------------

to use the whole appversion library, you should follow the following structure in laravel:

```php
$bundleId = 'package id'
$api= new Version;
$android = $api->getAndroid( $bundleId );
$ios = $api->getIos();
$data = [$android, $ios];

return $data;
```
obs: to use this lib in pure php you must add require 'vendor/autoload.php' in the AppVersion.php file


